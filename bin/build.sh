#!/usr/bin/env sh

set -e

PROJECT_ROOT="$( cd "$(dirname "$0")"/.. ; pwd -P )"

echo "PROJECT_ROOT=${PROJECT_ROOT}"

cd ${PROJECT_ROOT}/docker-context

DEBIAN_BASE_TAG=9.6
EXTRA_PKG_LIST="g++ gdb"
CB_VERSION=17.12

TAG=deb-${DEBIAN_BASE_TAG}-gpp-cb-${CB_VERSION}

IMG_NAME=codeblocks

LOCAL_TAG=${IMG_NAME}:${TAG}

(set -x; docker build -t ${LOCAL_TAG}  \
    --build-arg DEBIAN_BASE_TAG=${DEBIAN_BASE_TAG} \
    --build-arg EXTRA_PKG_LIST="${EXTRA_PKG_LIST}" \
    --build-arg CB_VERSION=${CB_VERSION} \
    .)

PUBLIC_TAGS=
PUBLIC_TAGS="${PUBLIC_TAGS} dostar/${IMG_NAME}:${TAG}"
PUBLIC_TAGS="${PUBLIC_TAGS} dostar/${IMG_NAME}"

for PUBLIC_TAG in ${PUBLIC_TAGS}; do
    (set -x; docker tag ${LOCAL_TAG} ${PUBLIC_TAG})
    (set -x; docker push ${PUBLIC_TAG})
done
