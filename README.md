# CodeBlocks

Start on MacOS:

```
IP=$(ipconfig getifaddr en0)
xhost + ${IP}

docker run -it --rm --network host \
	-e DISPLAY=${IP}:0 \
	-e HOME -e USER \
	-v${HOME}:${HOME} \
	--workdir ${HOME} \
	--user $(id -u) \
	dostar/codeblocks
```